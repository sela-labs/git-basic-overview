# Git for TFS users
Lab 01: Git Basics Summary

---

# Tasks

 - Visualize a standard merge
 
 - Visualize a fast-forward merge
  
 - Visualize a no-ff merge

 - Visualize a rebase
 
 - Visualize a pull-merge
 
 - Visualize a pull-rebase

---

## Preparations

 - Browse to the visualizing-git page and choose "Free Explore":

```
http://git-school.github.io/visualizing-git
```

- Clean the editor (if necessary):

```
$ clear
```

---


## Working with branches and tags

 - Create and checkout a new branch (feature1):
```
$ git checkout -b feature1
```

 - Create 2 commits in the feature1 branch:
```
$ git commit
$ git commit
```

 - Mark the previous commit with the "fix" tag:
```
$ git checkout HEAD~1
$ git tag fix
````

 - Move to the master branch:
```
$ git checkout master
```

## Undoing local changes

 - Create and checkout a new branch (feature2):
```
$ git checkout -b feature2
```

 - Create 2 commits in the feature2 branch:
```
$ git commit
$ git commit
```

 - Rollback the last commit:
```
$ git revert HEAD
````

 - Apply the changes in the "fix" commit:
```
$ git cherry-pick fix
````

 - Create a new commit in the feature2 branch:
```
$ git commit
```

 - Delete the last commit:
```
$ git reset --hard HEAD~1
```

 - Create a new commit in the feature2 branch:
```
$ git commit
```

&nbsp;
<img alt="Image 1.0" src="Images/1.0.PNG"  width="700" border="1">
&nbsp;

---

## Cleanup

 - Use the command below to clean the editor:
```
$ clear
```

---

## Visualize a standard merge

 - Create and checkout a new branch (feature1):
```
$ git checkout -b feature1
```

 - Create 2 commits in the feature1 branch:
```
$ git commit
$ git commit
```

 - Move to the master branch:
```
$ git checkout master
```

 - Create 2 commits in the master branch:
```
$ git commit
$ git commit
```

 - Merge the feature1 branch into the master branch:
```
$ git merge feature1
```

&nbsp;
<img alt="Image 1.1" src="Images/1.1.png"  width="700" border="1">
&nbsp;

---

## Visualize a fast-forward merge

 - Create and checkout a new branch (feature2):
```
$ git checkout -b feature2
```

 - Create 2 commits in the feature2 branch:
```
$ git commit
$ git commit
```

 - Move to the master branch:
```
$ git checkout master
```

 - Merge the feature2 branch into the master branch:
```
$ git merge feature2
```

&nbsp;
<img alt="Image 1.2" src="Images/1.2.png"  width="700" border="1">
&nbsp;

---

## Visualize a no-ff merge

 - Create and checkout a new branch (feature3):
```
$ git checkout -b feature3
```

 - Create 2 commits in the feature2 branch:
```
$ git commit
$ git commit
```

 - Move to the master branch:
```
$ git checkout master
```

 - Merge the feature3 branch into the master branch with --no-ff flag:
```
$ git merge feature3 --no-ff
```

&nbsp;
<img alt="Image 1.3" src="Images/1.3.png"  width="700" border="1">
&nbsp;

---

## Visualize a rebase

- Create and checkout a new branch (feature4):
```
$ git checkout -b feature4
```

 - Create 2 commits in the feature4 branch:
```
$ git commit
$ git commit
```

 - Create 2 commits in the master branch:
```
$ git checkout master
$ git commit
$ git commit
```

 - Rebase the feature branch into the master branch:
```
$ git checkout feature4
$ git rebase master
```

 - Move the master reference to the end of the tip:
```
$ git checkout master
$ git merge feature4
```

&nbsp;
<img alt="Image 1.4" src="Images/1.4.png"  width="700" border="1">
&nbsp;

---

## Cleanup and Preparations

- Select the "Upstream Changes" editor:

&nbsp;
<img alt="Image 1.5" src="Images/1.5.png"  width="350" border="1">
&nbsp;

---

## Visualize a pull-merge

 - Perform two commits in the feature branch:
```
$ git commit
$ git commit
```

 - Pull the remote commits using a regular pull:
```
$ git pull
```

&nbsp;
<img alt="Image 1.6" src="Images/1.6.png"  width="700" border="1">
&nbsp;

---

## Visualize a pull-rebase

 - Clean the editor:
```
$ clear
```

- Perform two commits in the feature branch:
```
$ git commit
$ git commit
```

 - Pull the remote commits using a regular pull:
```
$ git pull --rebase
```

&nbsp;
<img alt="Image 1.7" src="Images/1.7.png"  width="700" border="1">
&nbsp;